var boardDefinition;

var currentSelection = "";

const figures =
[
	"p", //pawn
	"n", //knight
	"r", //rook
	"b", //bishop
	"q", //queen
	"k"  //king
];

const teams =
[
	"w", //white
	"b"  //black
];



function secondsToTimestamp(seconds)
{
	if(seconds < 0)
		seconds = 0;
	seconds = Math.ceil(seconds);

	let hours = Math.floor(seconds / (60 * 60));
	seconds = seconds - hours * 60 * 60;

	let minutes = Math.floor(seconds / 60);
	seconds = seconds - minutes * 60;

	let result = "";
	if(hours)
		result += hours + ":";

	result += (minutes > 9 ? minutes : "0" + minutes) + ":";

	result += seconds > 9 ? seconds : "0" + seconds;

	return result;
}



function makeBoard(width, height)
{
	boardDefinition = new Object();
	boardDefinition.fields = [];
	boardDefinition.width = width;
	boardDefinition.height = height;
	let board = document.querySelector(".chess-container");
	board.innerHTML = "";

	board.style.gridTemplateColumns = `repeat(${width}, minmax(0, 1fr))`

	let fieldCount = width * height;
	for (let i = 0; i < fieldCount; i++)
	{
		boardDefinition.fields[i] = new Object();
		boardDefinition.fields[i].figure = null;
		boardDefinition.fields[i].removed = false;
		let field = document.createElement("button");
		if(i % width == 0)
		{
			let row = i / width;
			if(row % 2)
				boardDefinition.fields[i].type = "black";
			else
				boardDefinition.fields[i].type = "white";
		}
		else
		{
			if(boardDefinition.fields[i - 1].type == "black")
				boardDefinition.fields[i].type = "white";
			else
				boardDefinition.fields[i].type = "black";

		}
		field.onclick = function() { setField(field.id) }
		field.className += "chess-field " + boardDefinition.fields[i].type;
		field.id = i.toString();

		board.appendChild(field);
	}
}

function changeSize(percent)
{
	let board = document.querySelector(".chess-container");
	let width = board.style.width.replace("%", "");
	let widthInt = parseInt(width);
	board.style.width = (widthInt + percent) + "%";
}

function setField(id)
{
	let field = document.getElementById(id.toString());
	if(currentSelection == "")
	{
		if(!boardDefinition.fields[id].removed)
		{
			boardDefinition.fields[id].removed = true;
			field.setAttribute("removed", "");
			boardDefinition.fields[id].figure = null;
			field.style.backgroundImage = "";
		}
		else
		{
			boardDefinition.fields[id].removed = false;
			field.removeAttribute("removed");
		}
	}
	else if(currentSelection == "x")
	{
		boardDefinition.fields[id].figure = null;
		field.style.backgroundImage = "";
	}
	else
	{
		field.removeAttribute("removed");
		boardDefinition.fields[id].figure = currentSelection;
		field.style.backgroundImage = `url(/images/chess/${currentSelection[0]}/${currentSelection[1]}.png)`;
	}
}

function changeSelection(pressedFigure)
{
	let newSelection = pressedFigure.getAttribute("figure");
	if(newSelection == currentSelection)
	{
		currentSelection = "";
		for(let figureButton of document.querySelectorAll(".figure-button"))
			figureButton.removeAttribute("selected");
	}
	else
	{
		currentSelection = newSelection;
		for(let figureButton of document.querySelectorAll(".figure-button"))
			figureButton.removeAttribute("selected");
		pressedFigure.setAttribute("selected", "");
	}
}

function recreateBoard(boardInfo)
{
	let width = boardInfo.width;
	let height = boardInfo.height;
	let board = document.querySelector(".chess-container");
	board.innerHTML = "";

	board.style.gridTemplateColumns = `repeat(${width}, minmax(0, 1fr))`
	let fieldCount = width * height;

	for (let i = 0; i < fieldCount; i++)
	{
		let fieldInfo = boardInfo.fields[i];
		let field = document.createElement("button");

		field.className += "chess-field " + fieldInfo.type;
		field.id = i.toString();
		field.setAttribute("onclick", `fieldClick(${field.id})`);
		if(fieldInfo.figure)
			field.style.backgroundImage = `url(/images/chess/${fieldInfo.figure[0]}/${fieldInfo.figure[1]}.png)`;
		if(fieldInfo.removed)
			field.setAttribute("removed", "");
		board.appendChild(field);
	}
}

