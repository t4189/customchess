const express = require('express');

class Controller
{
	#router;
	constructor()
	{
		this.#router = express.Router();
	}

	getRouter()
	{
		return this.#router;
	}


	// For areas accessible only to users who are logged in
	static userAuthLevel(req, res, next)
	{
		if(req.session.user == undefined)
		{
			res.redirect("/login")
		}
		else
		{
			next();
		}
	}

	// For areas only accessible to players
	static playerAuthLevel(req, res, next)
	{
	    if (req.session.user == undefined)
		{
			res.redirect("/login");
		}
		else if(req.session.user.isAdmin)
		{
			res.redirect("/admin");
		}
		else
		{
	        next();
		}
    }

    // For areas only accessible to admins
    static adminAuthLevel(req, res, next)
	{
	    if (req.session.user == undefined)
	    {
			res.redirect('/login');
	    }
	    else if(!req.session.user.isAdmin)
	    {
	    	res.status(403).send("This area is for administrators only.");
	    }
	    else
	    {
	        next();
		}
    }

    // For redirecting players and admins to their respective areas of the site
    static indexRedirector(req, res, next)
    {
    	if(req.session.user == undefined || !req.session.user.isAdmin)
    	{
			res.redirect("/home");
    	}
		else
		{
			res.redirect("/admin");
		}
    }

    // For redirecting admins away from public areas of the website, since they only need to use the control panel
    static adminRedirector(req, res, next)
    {
    	if(req.session.user != undefined && req.session.user.isAdmin)
    		res.redirect("/admin")
    	else
    		next();
    }
}

module.exports = Controller;