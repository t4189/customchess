const express = require('express');
const path = require('path');
const pg = require('pg');
const session = require('express-session');
const pgSession = require('connect-pg-simple')(session);
const Variant = require('../models/Variant.js');
const http = require('http');
const RegisterController = require("../controllers/RegisterController.js");
const Register = new RegisterController();
const LoginController = require("../controllers/LoginController.js");
const Login = new LoginController();
const CreateController = require("../controllers/CreateController.js");
const Create = new CreateController();
const VariantController = require("../controllers/VariantController.js");
const VariantC = new VariantController();
class Server {

    constructor()
    {
        //Create node application
        const app = express();
        var server = http.Server(app);
        var socketIO = require('socket.io');
        var io = socketIO(server);
        global.io = io;
        //Set database object as global variable
        const Database = require("./Database.js");
        global.db = new Database();

        // Express Setup
        app.use(express.json())
        app.set('viewiews', path.join(__dirname, 'views'));
        app.set('view engine', 'ejs');
        app.use(express.static(path.join(__dirname, '/../public')));
        app.use(express.urlencoded({ extended: true }));

        // Session Setup
        app.use(session({
            store: new pgSession({ pool: db.getPool() }),
            secret: "topsecret",
            resave: false,
            saveUninitialized: true,
            unset: 'destroy',
            cookie: {
                secure: false
            }
        }));

        // Set up routing
        var Controller = require("../classes/Controller.js");
        app.get("/", Controller.indexRedirector);

        const controllers =
        [
            { "name":"Home",         "path":"/home",     },
            { "name":"Profile",      "path":"/profile",  },
            { "name":"Login",        "path":"/login",    },
            { "name":"Register",     "path":"/register", },
            { "name":"Create",       "path":"/create",   },
            { "name":"Variant",      "path":"/variant",  },
            { "name":"Play",         "path":"/play",     },
            { "name":"Admin",        "path":"/admin",    },
            { "name":"Invite",       "path":"/invite",   },
            { "name":"Confirm",      "path":"/confirm",  },
        ];

        for(let controllerInfo of controllers)
        {
            Controller = require("../controllers/" + controllerInfo.name + "Controller.js");
            let controller = new Controller();
            let router = controller.getRouter();

            app.use(controllerInfo.path, router);
        }


        // Set up port listening, process.env.PORT set by Heroku when publishing
        const PORT = process.env.PORT || 3000;


        // Recreate variant images deleted by Heroku ephemeral file system before allowing connections.
        Variant.recreateVariantImages().then(() =>
        {
            server.listen(PORT);
            console.log("Listening on port " + PORT);
        })
        //Unit test
        this.unitTests();
    }

    //Methods for unit tests
    async unitTests(){
        await this.testPasswordOnlySpaces();
        await this.testRegisterWithExistingCredentials();
        await this.testUsernameWithSpaces();
        await this.testPasswordWithSpaces();
        await this.testInvalidEmailFormat();
        await this.testRegisterShortCredentials();
        await this.testRegisterLongCredentials();
        await this.testDeactivatedLogin();
        await this.testWrongCredentialsLogin();
        await this.testNotConfirmedLogin();
        await this.testCommentTooLong();
        await this.testCommentSpacesOnly();
        await this.testTwoKingsVariant();
        await this.testNegativeDurationVariant();
        await this.testShortTitleVariant();
        
    }
    async testPasswordOnlySpaces(){
        
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING0] " + output)};
        req.body = {
            "username":"UTUser" + Math.floor(Math.random() * 100000),
            "password":"          ",
            "email":"asd" + Math.floor(Math.random() * 100000) + "@gmail.com"
        }
        
        console.log("[TESTING0]Testing register with params:")
        console.log(req);
        console.log("[TESTING0] Errors:");
        await Register.register(req, res, {});
        console.log("[TESTING0] Test done")
    }
    async testDeactivatedLogin(){
        
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING7] " + output)};
        req.body = {
            "usernameOrEmail":"janko",
            "password": "password"
        }
        
        console.log("[TESTING7]Testing login with params:")
        console.log(req);
        console.log("[TESTING7] Errors:");
        await Login.login(req, res, {});
        console.log("[TESTING7] Test done")
    }

    async testWrongCredentialsLogin(){
        
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING8] " + output)};
        req.body = {
            "usernameOrEmail":"LTUser" + Math.floor(Math.random() * 100000),
            "password":"asdafg" + Math.floor(Math.random() * 100000)
        }
        
        console.log("[TESTING8]Testing login with params:")
        console.log(req);
        console.log("[TESTING8] Errors:");
        await Login.login(req, res, {});
        console.log("[TESTING8] Test done")
    }
    async testNotConfirmedLogin(){
        
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING9] " + output)};
        req.body = {
            "usernameOrEmail":"NoTConfirmed",
            "password":"password"
        }
        
        console.log("[TESTING9]Testing register with params:")
        console.log(req);
        console.log("[TESTING9] Errors:");
        await Login.login(req, res, {});
        console.log("[TESTING9] Test done")
    }

    async testCommentTooLong() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING10] " + output)};
        req.body = {
            "comment":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        }
        req.params = {
            "id":"80"
        }
        req.session = {
            "user": JSON.parse('{"id":"81"}')
        }

        console.log("[TESTING10]Testing register with params:")
        console.log(req);
        console.log("[TESTING10] Errors:");
        await VariantC.commentVariant(req, res, {});
        console.log("[TESTING10] Test done");
    }

    async testCommentSpacesOnly() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING11] " + output)};
        req.body = {
            "comment":"      "
        }
        req.params = {
            "id":"80"
        }
        req.session = {
            "user": JSON.parse('{"id":"81"}')
        }

        console.log("[TESTING11]Testing register with params:")
        console.log(req);
        console.log("[TESTING11] Errors:");
        await VariantC.commentVariant(req, res, {});
        console.log("[TESTING11] Test done");
    }


    async testTwoKingsVariant(){
        
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING12] " + output)};
        req.body = {
            "title":"Two King Variant",
            "duration": "120",
            "extension": "0",
            "privacy": "true",
            "board": JSON.parse('{"fields":[{"figure":"wk","removed":false,"type":"white"},{"figure":"wk","removed":false,"type":"black"},{"figure":"bp","removed":false,"type":"black"},{"figure":"bk","removed":false,"type":"white"}],"width":"2","height":"2"}')

        }
        req.session = {
            "user": JSON.parse('{"id":"79"}')
        }
        
        console.log("[TESTING12]Testing create with params:")
        console.log(req);
        console.log("[TESTING12] Errors:");
        await Create.createVariant(req, res, {});
        console.log("[TESTING12] Test done")
    }
    
    async testNegativeDurationVariant(){
        
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING13] " + output)};
        req.body = {
            "title":"Neg Duration Variant",
            "duration": "-120",
            "extension": "0",
            "privacy": "true",
            "board": JSON.parse('{"fields":[{"figure":"wk","removed":false,"type":"white"},{"figure":"wp","removed":false,"type":"black"},{"figure":"bp","removed":false,"type":"black"},{"figure":"bk","removed":false,"type":"white"}],"width":"2","height":"2"}')

        }
        req.session = {
            "user": JSON.parse('{"id":"79"}')
        }
        
        console.log("[TESTING13]Testing create with params:")
        console.log(req);
        console.log("[TESTING13] Errors:");
        await Create.createVariant(req, res, {});
        console.log("[TESTING13] Test done")
    }

    async testShortTitleVariant(){
        
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING14] " + output)};
        req.body = {
            "title":"VT",
            "duration": "120",
            "extension": "0",
            "privacy": "true",
            "board": JSON.parse('{"fields":[{"figure":"wk","removed":false,"type":"white"},{"figure":"wp","removed":false,"type":"black"},{"figure":"bp","removed":false,"type":"black"},{"figure":"bk","removed":false,"type":"white"}],"width":"2","height":"2"}')

        }
        req.session = {
            "user": JSON.parse('{"id":"79"}')
        }
        
        console.log("[TESTING14]Testing create with params:")
        console.log(req);
        console.log("[TESTING14] Errors:");
        await Create.createVariant(req, res, {});
        console.log("[TESTING14] Test done")
    }

    async testRegisterWithExistingCredentials() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING1] " + output)};
        req.body = {
            "username":"pero5",
            "password":"peropero",
            "email":"peroperic@gmail.com"
        }
        //console.log(Register);
        console.log("[TESTING1]Testing register with params:")
        console.log(req);
        console.log("[TESTING1] Errors:");
        await Register.register(req, res, {});
        console.log("[TESTING1] Test done");
    }

    async testUsernameWithSpaces() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING2] " + output)};
        req.body = {
            "username":"a    a",
            "password":"abcdefgh",
            "email":"asd" + Math.floor(Math.random() * 100000) + "@gmail.com"
        }
        //console.log(Register);
        console.log("[TESTING2]Testing register with params:")
        console.log(req);
        console.log("[TESTING2] Errors:");
        await Register.register(req, res, {});
        console.log("[TESTING2] Test done");
    }

    async testPasswordWithSpaces() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING3] " + output)};
        req.body = {
            "username":"UTUser" + Math.floor(Math.random() * 100000),
            "password":"a      a",
            "email":"asd" + Math.floor(Math.random() * 100000) + "@gmail.com"
        }
        //console.log(Register);
        console.log("[TESTING3]Testing register with params:")
        console.log(req);
        console.log("[TESTING3] Errors:");
        await Register.register(req, res, {});
        console.log("[TESTING3] Test done");
    }

    async testInvalidEmailFormat() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING4] " + output)};
        req.body = {
            "username":"UTUser" + Math.floor(Math.random() * 100000),
            "password":"abcdefgh",
            "email":"asd" + Math.floor(Math.random() * 100000)
        }
        //console.log(Register);
        console.log("[TESTING4]Testing register with params:")
        console.log(req);
        console.log("[TESTING4] Errors:");
        await Register.register(req, res, {});
        console.log("[TESTING4] Test done");
    }

    async testRegisterShortCredentials() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING5] " + output)};
        req.body = {
            "username":"a",
            "password":"b",
            "email":"asd" + Math.floor(Math.random() * 100000) + "@gmail.com"
        }
        //console.log(Register);
        console.log("[TESTING5]Testing register with params:")
        console.log(req);
        console.log("[TESTING5] Errors:");
        await Register.register(req, res, {});
        console.log("[TESTING5] Test done");
    }

    async testRegisterLongCredentials() {
        var req = {};
        var res = {};
        res.send = function(output){console.log("[TESTING6] " + output)};
        req.body = {
            "username":"aaaaaaaaaaaaaaaaaaaaaaaaaa",
            "password":"bbbbbbbbbbbbbbbbbbbbbbbbbb",
            "email":"asd" + Math.floor(Math.random() * 100000) + "@gmail.com"
        }
        //console.log(Register);
        console.log("[TESTING6]Testing register with params:")
        console.log(req);
        console.log("[TESTING6] Errors:");
        await Register.register(req, res, {});
        console.log("[TESTING6] Test done");
    }

}

module.exports = Server;