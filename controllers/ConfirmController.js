const Player = require('../models/Player.js');
const Controller = require('../classes/Controller.js');

class ConfirmController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.get('/:code', this.confirm);
	}

	async confirm(req, res, next)
	{
		let success = await Player.confirmEmail(req.params.code);
		let message = success ? "Email confirmed. You can now log in." : "Invalid link.";
		res.render("confirm", {user: req.session.user, message: message });
	}
}

module.exports = ConfirmController;