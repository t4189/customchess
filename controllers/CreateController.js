const Controller = require('../classes/Controller.js');
const Variant = require('../models/Variant');

class CreateController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.use(Controller.playerAuthLevel);
		router.get('/', this.getCreate);
		router.post('/', this.createVariant);
	}

	getCreate(req, res, next)
	{
		res.render("create", {user: req.session.user});
	}

	async createVariant(req, res, next)
	{
		let creatorId = req.session.user.id;
		let title     = req.body.title;
		let duration  = req.body.duration;
		let extension = req.body.extension;
		let privacy   = req.body.privacy;
		let board     = req.body.board;

		let errors = [];

		// Check if we have exactly one of each king
		let whiteKing = false;
		let blackKing = false;
		if(board == undefined)
		{
			errors.push("Please define a board for your variant.");
		}
		else
		{
			for(let field of board.fields)
			{
				if(field.figure == "wk")
				{
					if(whiteKing)
					{
						errors.push("The board needs to have exactly one white and one black king.");
						break;
					}
					whiteKing = true;
				}

				if(field.figure == "bk")
				{
					if(blackKing)
					{
						errors.push("The board needs to have exactly one white and one black king.");
						break;
					}
					blackKing = true;
				}
			}

			if(!whiteKing || !blackKing)
				errors.push("The board needs to have exactly one white and one black king.")
		}

		if(duration < 1)
			errors.push("Time limit must be at least 1 second.");
		if(extension < 0)
			errors.push("Time gained per move can't be negative.");
		if(title.length < 5 || title.length > 30)
			errors.push("Title must be between 5 and 30 characters.");

		// Save variant to database and create image for it
		if(errors.length == 0)
		{
			var variantId = await Variant.createVariant(creatorId, title, duration, extension, privacy, board);

			const fs = require('fs')
			const { createCanvas, loadImage } = require('canvas');
			const width = 800;
			const height = 1000;
			const heightForBoard = 800;

			var canvas = createCanvas(width, height);
			var context = canvas.getContext('2d');

			var fieldWidth  = Math.floor(width  / board.width);
			var fiedlHeight = Math.floor(heightForBoard / board.height);
			var fieldSize;

			if(fieldWidth > fiedlHeight)
			    fieldSize = fiedlHeight;
			else
			    fieldSize = fieldWidth;

			var xOffset = Math.floor((width - fieldSize * board.width) / 2);
			var yOffset = Math.floor((heightForBoard - fieldSize * board.height) / 2);

			var black = "#b58863";
			var white = "#f0d9b5";
			var removed = "grey";

			let figureImages = [];

			// Fill background
			context.fillStyle = "#fccc74";
			context.fillRect(0, 0, width, height);
			// Fill area for board
			context.fillStyle = "#855E42";
			context.fillRect(0, 0, width, heightForBoard);

			for(let i = 0; i < board.width * board.height; i++)
			{
			    let field = board.fields[i];

			    if(field.type == "white")
			        context.fillStyle = white;
			    else if(field.type == "black")
			        context.fillStyle = black;
			    if(field.removed)
			        context.fillStyle = removed;

			    let x = xOffset + fieldSize * (i % board.width);
			    let y = yOffset + fieldSize * Math.floor(i / board.width);

			    context.fillRect(x, y, fieldSize, fieldSize);

			    let figure = field.figure;
			    if(figure)
			    {
			        if(figureImages[figure] == undefined)
			        {
			            figureImages[figure] = await loadImage(`./public/images/chess/${figure[0]}/${figure[1]}.png`);
			        }
			        context.drawImage(figureImages[figure], x, y, fieldSize, fieldSize);
			    }
			}

			var timeFormat = `${duration}s + ${extension}s`

			context.fillStyle = "#000000";
			context.font = "64px Arial";
			context.textAlign = "center";
			context.textBaseline = 'middle';
			context.fillText(timeFormat, width / 2, heightForBoard + (height-heightForBoard) / 2);

			const buffer = canvas.toBuffer('image/png');
			fs.writeFileSync(`./public/images/variant/${variantId}.png`, buffer);
		}

		res.send(JSON.stringify(errors));
	}
}

module.exports = CreateController;