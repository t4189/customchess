const Controller = require('../classes/Controller.js');
const validator = require("email-validator");
const Player = require('../models/Player');

class RegisterController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter()
		router.get('/', this.getRegister);
		router.post('/', this.register);
	}

	getRegister(req, res, next)
	{
		res.render("register", {user: req.session.user});
	}

	async register(req, res, next)
	{
		let username = req.body.username.trim();
		let password = req.body.password.trim();
		let email    = req.body.email.trim();
		let errors = [];

		// Validate username
		if (!(username.length >= 5 && username.length <= 25))
			errors.push("Username must be between 5 and 25 characters.");
		if (username.includes(" "))
			errors.push("Username can't contain spaces");
		let usernameFree = await Player.checkUsernameAvailability(username);
		if (!usernameFree)
			errors.push("Username already in use.");

		// Validate password
		if (!(password.length >= 8 && password.length <= 25))
			errors.push("Password must be between 8 and 25 characters.");
		if (password.includes(" "))
			errors.push("Password can't contain spaces");

		// Validate email
		if (!validator.validate(email))
			errors.push("Please choose a valid email.");
		let emailFree = await Player.checkEmailAvailability(email);
		if (!emailFree)
			errors.push("Email already in use");

		if (!errors.length)
		{
			let user = await Player.createPlayer(username, password, email);
			//req.session.user = user;
			await req.session.save();
		}

		res.send(JSON.stringify(errors));
	}
}

module.exports = RegisterController;