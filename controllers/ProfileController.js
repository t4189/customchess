const Controller = require('../classes/Controller.js');
const Variant = require('../models/Variant');
const Player = require('../models/Player');

class ProfileController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.use(Controller.playerAuthLevel);
		router.get('/', this.index);
		router.post('/delete', this.deleteVariant);
		router.post('/unfav', this.removeFromFavourites);
		router.post('/privacy', this.toggleVariantPrivacy);
	}

	async index(req, res, next)
	{
		let id = req.session.user.id
		let own = await Variant.getOwnVariants(id);
		let favs = await Variant.getFavouriteVariants(id);
		let stats = await Player.getStats(id);

		res.render("profile", {user: req.session.user, own: own.rows, favs: favs.rows, wins: stats.wins, losses: stats.losses});
	}

	async deleteVariant(req, res, next)
	{
		let variants = await Variant.deleteVariant(req.body.id);
		res.send(JSON.stringify(variants));

	}

	async toggleVariantPrivacy(req, res, next)
	{
		let variants = await Variant.toggleVariantPrivacy(req.body.id);
		res.send(JSON.stringify(variants));
	}
	async removeFromFavourites(req, res, next)
	{
		let variants = await Variant.removeFromFavourites(req.body.id, req.session.user.id);
		res.send(JSON.stringify(variants));
	}
}

module.exports = ProfileController;