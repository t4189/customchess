const Controller = require('../classes/Controller.js');
const Variant = require('../models/Variant');
const Comment = require('../models/Comment');

class VariantController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.use(Controller.adminRedirector);
		router.get('/:id([0-9]{1,10})', this.getVariant);
		router.post('/:id([0-9]{1,10})', Controller.playerAuthLevel, this.commentVariant);
		router.post('/Like', Controller.playerAuthLevel, Controller.userAuthLevel, this.LikeVariant);
		router.post('/addtofav', Controller.playerAuthLevel, Controller.userAuthLevel, this.addtofav);
		router.post('/removefromfav', Controller.playerAuthLevel, Controller.userAuthLevel, this.removefromfav);
		router.post('/unLike', Controller.playerAuthLevel, Controller.userAuthLevel, this.unLikeVariant);
		router.post('/Dislike', Controller.playerAuthLevel, Controller.userAuthLevel, this.DislikeVariant);
		router.post('/unDislike', Controller.playerAuthLevel, Controller.userAuthLevel, this.unDislikeVariant);
	}

	async getVariant(req, res, next)
	{
		//await Variant.deleteFromTable() //Ovo je za brisanje lajkova, dislajkova i favourita za sve varijante
    	let variantId = parseInt(req.params.id);
    	let variant = await Variant.getVariant(variantId);
		console.log(variant)
		let stats = []

		if(req.session.user!=undefined){
			stats = await Variant.getStats(req.session.user.id, variantId)
		}
		else{
			stats[0] = ""
			stats[1] = ""
			stats[2] = ""
		}
		let comments = await Comment.getCommentsForVariant(variantId);

    	if (variant == null)
    	{
    		res.sendStatus(404);
    	}
    	else
    	{
    		res.render("variant", {user: req.session.user, variant: variant, comments: comments, liked: stats[0], disliked: stats[1], fav: stats[2]});
    		Variant.incrementViews(variantId);
    	}
	}


	async LikeVariant(req, res, next)
	{
		//console.log("evo me");
		let variantId = parseInt(req.body.id);
		let dobar = await Variant.LikeVariant(variantId, req.session.user.id)
		console.log("iz controllera" + dobar)

		res.send(JSON.stringify(dobar));
	}

	async unLikeVariant(req, res, next)
	{
		console.log("uso sam u unLikedcontrolelr");
    	let variantId = parseInt(req.body.id);
		let variant = await Variant.unLikeVariant(variantId, req.session.user.id)

		res.send(JSON.stringify(variant));
	}

	async DislikeVariant(req, res, next)
	{
		console.log("Dislike evo me hehe");
		let variantId = parseInt(req.body.id);
		let variant = await Variant.DislikeVariant(variantId, req.session.user.id)


		res.send(JSON.stringify(variant));

	}

	async unDislikeVariant(req, res, next)
	{
		console.log("usoSamU_undislajk")
		let variantId = parseInt(req.body.id);
		let variant = await Variant.unDislikeVariant(variantId, req.session.user.id)


		res.send(JSON.stringify(variant));
	}

	async commentVariant(req, res, next)
	{
		let variantId = parseInt(req.params.id);
		let variant = await Variant.getVariant(variantId);

		let userId = req.session.user.id;
		let errors = [];
		var text = req.body.comment.trim();
		console.log(text.length);
		if(text.length < 5 || text.length > 280) {
			errors.push("Comment must have between 5 and 280 characters.")
		}else {
			await Comment.createComment(variantId, userId, text);
		}
		
		let comments = await Comment.getCommentsForVariant(variantId);

		let stats = []

		if(req.session.user!=undefined){
			stats = await Variant.getStats(userId, variantId)
		}
		else{
			stats[0] = ""
			stats[1] = ""
		}

		//res.render("variant", {user: req.session.user, variant: variant, comments: comments, Liked: stats[0], Disliked: stats[1]});
		if(!errors.length) {
			res.redirect("/variant/"+ variantId);
		}
		if(errors.length) {
			res.send(JSON.stringify(errors));
		}
		
	}

	async addtofav(req, res, next)
	{
		let variantId= parseInt(req.body.id)
		let variant = await Variant.favouriteVariant(variantId, req.session.user.id)

		res.send(JSON.stringify(variant));
	}

	async removefromfav(req,res,next){
		
		let variantId= parseInt(req.body.id)
		
		let variant = await Variant.removeFromFavourites(variantId, req.session.user.id)

		res.send(JSON.stringify(variant));
	}
}

module.exports = VariantController;