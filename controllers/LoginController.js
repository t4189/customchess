const Controller = require('../classes/Controller.js');
const User = require('../models/User');

class LoginController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.get('/logout', Controller.userAuthLevel, this.logout);
		router.get('/', this.getLogin);
		router.post('/', this.login);
	}

	getLogin(req, res, next)
	{
		res.render("login", {user: req.session.user});
	}
	async login(req, res, next)
	{
		let usernameOrEmail = req.body.usernameOrEmail.trim();
		let password = req.body.password.trim();

		let errors = [];
		let user = await User.getUserByCredentials(usernameOrEmail, password);

		if(user == null)
			errors.push("Invalid credentials.");
		else if(user.deactivated)
			errors.push("Your account is currently deactivated.")
		else if(!user.confirmed)
			errors.push("You need to confirm your email address before logging in.")

		if(!errors.length)
		{
			req.session.user = user;
			await req.session.save();
		}

		res.send(JSON.stringify(errors));
	}
	async logout(req, res, next)
	{
		req.session.user = undefined;
		res.redirect("/");
	}
}

module.exports = LoginController;