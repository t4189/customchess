const Controller = require('../classes/Controller.js');
const Variant = require('../models/Variant');


class HomeController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.use(Controller.adminRedirector);
		router.get('/', this.getHomePage);
	}

	async getHomePage(req, res, next)
	{
		//var variantIds = await Variant.getAllPublicVariantIds();

		var bestRated = await Variant.getBestRatedVariantIds(3)
		var mostPopular = await Variant.getMostPopularVariantIds(3)
		var newest = await Variant.getNewestVariantIds(6)

		res.render("home", {user: req.session.user, bestRated, mostPopular, newest});
	}
}

module.exports = HomeController;