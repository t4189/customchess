const Controller = require('../classes/Controller.js');
const Invite = require('../models/Invite');

class InviteController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.use(Controller.playerAuthLevel);
		router.get('/', this.getInvites);
		router.post('/delete', this.deleteInvite);
	}

	async getInvites(req, res, next)
	{
		var invites = await Invite.getReceivedInvites(req.session.user.id);
		console.log(invites);
		res.render("invite", {user: req.session.user, invites: invites});
	}

	async deleteInvite(req, res, next)
	{
		var id = req.body.id;
		await Invite.deleteInvite(id);
		res.send(JSON.stringify({success: true}));
	}

}

module.exports = InviteController;