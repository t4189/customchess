const Controller = require('../classes/Controller.js');
const Variant = require('../models/Variant');
const Invite = require('../models/Invite');
const User = require('../models/User');
const Player = require('../models/Player');
const http = require('http');

class PlayController extends Controller
{
	constructor()
	{
		super();

		var router = this.getRouter();
		router.use(Controller.playerAuthLevel);
		router.get('/:id([0-9]{1,10})', this.playVariant);
		router.get('/:id([0-9]{1,10})/:senderId([0-9]{1,10})', this.playVariant);
		router.post('/invite', this.invite);

		const matches = {};
		global.matches = matches;

		var io = global.io;
		io.on("connection", function (socket)
		{
			console.log("Connection");
			let senderId = socket.handshake.query.senderId;
			let firstPlayer = socket.handshake.query.firstPlayer === 'true';

			if(firstPlayer)
			{
				matches[senderId] = {};
				matches[senderId].firstPlayer = socket.id;
				matches[senderId].firstPlayerId = senderId;
			}
			else
			{
				if(matches[senderId])
				{
					matches[senderId].secondPlayer = socket.id;
				}
			}

			socket.join(senderId);

			socket.on("secondPlayerConnected", function(username, id)
			{
				if(matches[senderId])
				{
					matches[senderId].secondPlayerId = id;
				}
				console.log("Second player connected: " + username);
				io.to(senderId).emit("secondPlayerConnected", username);
			});

			socket.on("timeUpdate", function(firstTime, secondTime)
			{
				//console.log("Time update: " + firstTime + " " + secondTime);
				io.to(senderId).emit("timeUpdate", firstTime, secondTime);
			});

			// sender: 1 - first player, 2 - second player
			socket.on("turn", function(sender, idFrom, idTo)
			{
				console.log("Player " + sender + " made turn");
				io.to(senderId).emit("turn", sender, idFrom, idTo);
			});

			socket.on("finished", function(winner, variantId)
			{
				console.log("!!!VariantID is: " + variantId)
				let winnerId = 0, loserId = 0;

				if(matches[senderId])
				{
					if(winner == 1)
					{
						winnerId = matches[senderId].firstPlayerId;
						loserId = matches[senderId].secondPlayerId;
					}
					else
					{
						winnerId = matches[senderId].secondPlayerId;
						loserId = matches[senderId].firstPlayerId;
					}
				}

				io.to(senderId).emit("finished", winner);

				Player.updateStats(winnerId, loserId);
				Variant.incrementPlays(variantId);

				//send stats
				let response = acceptStatsEmulate("chess-statistics-server.com/accept", "POST", {winnerId: winnerId, loserId: loserId, variantId: variantId});
				console.log("Sent match statistic and got response: " + response);

				function acceptStatsEmulate(url, method, body)
				{
					let winnerId  = body.winnerId;
					let loserId   = body.loserId;
			    	let variantId = body.variantId;
			    	console.log(`Received match statistic - winnerId: ${winnerId}, loserId: ${loserId}, variantId: ${variantId}`)
			    	return "Match statistic accepted.";
				}

				matches[senderId] = null;
			});

		});

		setInterval(() => {io.emit("message", {message: "helo"})}, 10000);
	}

	async playVariant(req, res, next)
	{
		let variantId = parseInt(req.params.id);
    	let variant = await Variant.getVariant(variantId);
    	let senderId = req.params.senderId;
    	let senderName = null;
	    if(senderId)
	    {
	    	let sender = await User.getUserById(senderId);
	    	senderName = sender.username;

	    }
	    console.log(global.matches)
    	if(senderId)
    	{
    		if(!global.matches[senderId])
    			return res.status(400).send("This invite is no longer available.");
    		if(!global.matches[senderId].firstPlayer || global.matches[senderId].secondPlayer)
    			return res.status(400).send("This invite is no longer available.");
    	}

		if (variant == null)
    	{
    		res.sendStatus(404);
    	}
    	else
    	{
    		res.render("play", {user: req.session.user, variant: variant, sender: senderId, senderName: senderName});
    		Variant.incrementViews(variantId);
    	}
	}

	async invite(req, res, next)
	{
		let username = req.body.username;
		let variantId = req.body.variantId;
    	let senderId = req.body.senderId;

    	let success = false;
    	success = await Invite.createInvite(username, senderId, variantId);

		let errors = [];

		if(!success)
			errors.push("Invalid username.");

		res.send(JSON.stringify(errors));
	}
}

module.exports = PlayController;