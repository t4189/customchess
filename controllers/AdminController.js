const Controller = require('../classes/Controller.js');
const User = require('../models/User');
const Comment = require('../models/Comment');
const Variant = require('../models/Variant');

class AdminController extends Controller
{
	constructor()
	{
		super();
		var router = this.getRouter();
		router.use(Controller.adminAuthLevel);
		router.get('/', this.getAdminPage);
		router.get('/users', this.getUsers);
		router.get('/variants', this.getVariants);
		router.get('/comments', this.getComments);
		router.post('/variants/del', this.deleteVariant);
		router.post('/users/reactivate', this.reactivateUser);
		router.post('/users/deactivate', this.deactivateUser);
		router.post('/comments/del', this.deleteComment);
	}

	getAdminPage(req, res, next)
	{
		res.render("admin", {user: req.session.user});
	}

	async getUsers(req, res, next)
	{
		let users = await User.getAllUsers();
		res.render("admin_users", {user: req.session.user, users: users});
	}

	async getVariants(req, res, next)
	{	
		let variants = await Variant.getAllVariants();
		//console.log(variants);
		res.render("admin_variants", {user: req.session.user, variants: variants});
	}

	async getComments(req, res, next)
	{
		let comments = await Comment.getAllComments();
		//console.log(comments);
		res.render("admin_comments", {user: req.session.user, comments: comments});
	}

	async deleteVariant(req, res, next)
	{
		let variants = await Variant.deleteVariant(req.body.id);
		res.send(JSON.stringify(variants));
		//res.redirect('/admin/variants');

	}

	async deleteComment(req, res, next)
	{
		let comments = await Comment.deleteComment(req.body.id);
		res.send(JSON.stringify(comments));

	}

	async deactivateUser(req, res, next)
	{	
		let users = await User.deactivateUser(req.body.id);
		res.send(JSON.stringify(users));
		//res.redirect('/admin/users');
	}

	async reactivateUser(req, res, next)
	{	
		let users = await User.reactivateUser(req.body.id);
		res.send(JSON.stringify(users));

	}
}

module.exports = AdminController;