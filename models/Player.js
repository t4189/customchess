const User = require('./User.js');
const { v4: uuidv4 } = require('uuid');
var SqlString = require('sqlstring');

class Player extends User
{
	static async createPlayer(username, password, email)
	{
		let confirmationCode = uuidv4();

		var nodemailer = require('nodemailer');
		var smtpTransport = require('nodemailer-smtp-transport');

		var transporter = nodemailer.createTransport(
		{
		    service: 'Gmail',
		    auth: {
			  	user: 'customchessPROGI@gmail.com',
				pass: '2My8cun9ki7cb9T'
		    }
		});

		let link = "https://customchess.herokuapp.com" + "/confirm/" + confirmationCode;

		var mailOptions =
		{
		  from: 'customchessPROGI@gmail.com',
		  to: email,
		  subject: 'CustomChess email confirmation',
		  text: "Go to the following link to confirm your address:\n" + link
		};

		transporter.sendMail(mailOptions, function(error, info)
		{
		  console.log("helo");
		  if (error)
		    console.log(error);
		  else
		    console.log('Email sent: ' + info.response);
		});


		let passwordHash = require("crypto").createHash("sha256").update(password).digest("hex");
		let query = `insert into account(username, password_hash, email, confirmation_code) values ('${username}','${passwordHash}','${email}', '${confirmationCode}') returning id;`
		let result = await global.db.query(query);

		let playerId = result.rows[0].id;

		query = `insert into player values(${playerId});`
		result = await global.db.query(query);

		return await User.getUserById(playerId);
	}

	static async checkEmailAvailability(email)
	{
		let query = "select * from account where email = '" + email + "'";
		let result = await global.db.query(query);

		return result.rowCount == 0;
	}
	static async checkUsernameAvailability(username)
	{

		let query = "select * from account where username = '" + username + "'";
		let result = await global.db.query(query);

		return result.rowCount == 0;
	}
	static async confirmEmail(code)
	{
		let query = "update account set confirmation_code = NULL where confirmation_code = " + SqlString.escape(code);
		let result = await global.db.query(query);
		return result.rowCount;
	}

	static async updateStats(winnerId, loserId)
	{
		let query  = "update player set wins = wins + 1 where id = " + winnerId;
		let result = await global.db.query(query);

		query = "update player set losses = losses + 1 where id =" + loserId;
		result = await global.db.query(query);
	}
	static async getStats(playerId)
	{
		let query = "select wins, losses from player where id = " + playerId;
		let result = await global.db.query(query);
		if(result.rows[0])
			return { wins: result.rows[0].wins, losses : result.rows[0].losses};
		else
			return null;
	}
}

module.exports = Player