class Comment
{
	constructor(id, creatorId, variantId, content)
	{
		this.id = id;
		this.creatorId = creatorId;
		this.variantId = variantId;
		this.content = content;
	}

	// for VariantController
	static async createComment(variantId, userId, commentContent)
	{
		let query = `insert into comment(variant_id, player_id, content)
					values ('${variantId}', '${userId}', '${commentContent}') returning id;`;
		let result = await global.db.query(query);
		

		
		//return result.rows[0].id;
	}

	// for VariantController
	static async getCommentsForVariant(variantId, userId)
	{
		let query = 'select comment.*, account.username from comment join account on (comment.player_id = account.id) where comment.variant_id = '+ variantId;
		let result = await global.db.query(query);
		if (result.rows.length==0)
			return null; 
		
		
		let comments = [];

		for (let r of result.rows) {
			comments.push(r);
		}
		
		return comments;
	}

	// for AdminController
	static async getAllComments()
	{
		let query = 'select * from comment';
		let result = await global.db.query(query);
		if(result.rows.length == 0)
			return null;

		let comments = [];
		for(let r of result.rows)
		{
			comments.push(new Comment(r.id, r.player_id, r.variant_id, r.content));
		}
		return comments;
	}

	// for AdminController
	static async deleteComment(id)
	{
		let query = `delete from comment where id = '${id}'`;
		let result = await global.db.query(query);
		return result;

	}
}
module.exports = Comment;