class User
{
	constructor(id, username, email, isAdmin, deactivated, confirmed)
	{
		this.id = id;
		this.username = username;
		this.email = email;
		this.isAdmin = isAdmin;
		this.deactivated = deactivated;
		this.confirmed = confirmed;
	}

	static async getUserById(id)
	{
		let query = `select account.*,
		             (administrator.id is not NULL) as is_admin
					 from account left join administrator on(account.id = administrator.id)
					 			  left join player on(account.id = player.id)
					 where account.id =` + id;
		let result = await global.db.query(query);
		if(result.length == 0)
			return null;
		let user = result.rows[0];
		return new User(user.id, user.username, user.email, user.is_admin, user.deactivated);
	}

	static async getUserByCredentials(usernameOrEmail, password)
	{
		let passwordHash = require("crypto").createHash("sha256").update(password).digest("hex");
		let query = `select account.*, player.*,
		             (administrator.id is not NULL) as is_admin,
		             confirmation_code IS NULL as confirmed
					 from account left join administrator on(account.id = administrator.id)
					 			  left join player on(account.id = player.id)
					 where (account.username = '${usernameOrEmail}' or account.email = '${usernameOrEmail}') and account.password_hash = '${passwordHash}'`;
		let result = await global.db.query(query);
		if(result.rows.length == 0)
			return null;
		let user = result.rows[0];
		return new User(user.id, user.username, user.email, user.is_admin, user.deactivated, user.confirmed);
	}

	// for AdminController
	static async getAllUsers()
	{
		let query = `select account.*,
		             (administrator.id is not NULL) as is_admin
					 from account left join administrator on(account.id = administrator.id)
					 			  left join player on(account.id = player.id)
					 order by id`;
		let result = await global.db.query(query);

		let users = [];

		for(let row of result.rows)
		{
			users.push(new User(row.id, row.username, row.email, row.is_admin, row.deactivated));
		}

		return users;
	}
	// for AdminController
	static async deactivateUser(id)
	{
		let query = `UPDATE account set deactivated = true where id = '${id}'`;
		let result = await global.db.query(query);
		return result;
	}
	// for AdminController
	static async reactivateUser(id)
	{
		let query = `UPDATE account set deactivated = false where id = '${id}'`;
		let result = await global.db.query(query);
		return result;
	}
}

module.exports = User;