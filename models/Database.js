const {Pool} = require('pg');

//postgres://kvstekmcjdrgcz:beafdf7d951e3378f69a0e7aefd371c620c66170ea01c1bea5714c3a3c4e4edd@ec2-54-208-17-82.compute-1.amazonaws.com:5432/dau9jj1lqaol4a

class Database
{
    #pool;
    constructor()
    {
        this.#pool = new Pool(
        {
            user: 'kvstekmcjdrgcz',
            host: 'ec2-54-208-17-82.compute-1.amazonaws.com',
            database: 'dau9jj1lqaol4a',
            password: 'beafdf7d951e3378f69a0e7aefd371c620c66170ea01c1bea5714c3a3c4e4edd',
            port: 5432,
            max: 20,
            ssl: { rejectUnauthorized: false }
        });
    }
    query(text, params)
    {
        console.log(text);
        return this.#pool.query(text, params)
        .then(res => {return res;});
    }
    getPool()
    {
        return this.#pool;
    }
}


module.exports = Database;
