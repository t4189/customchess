class Variant
{
	constructor(id, views, plays, Likes, Dislikes, board, privacy, title, duration, extension, creator)
	{
		this.id = id;
		this.views = views;
		this.plays = plays;
		this.Likes = Likes;
		this.Dislikes = Dislikes;
		this.board = board;
		this.privacy = privacy;
		this.title = title;
		this.duration = duration;
		this.extension = extension;
		this.creator = creator;
	}
	static async createVariant(creatorId, title, duration, extension, privacy, board)
	{
		let boardString = JSON.stringify(board);
		let query = `insert into variant(player_id, title, duration, extension, privacy, board)
					 values ('${creatorId}','${title}','${duration}','${extension}','${privacy}','${boardString}') returning id;`;

		let result = await global.db.query(query);
		return result.rows[0].id;
	}

	static async incrementPlays(variantId)
    {
    	let query = `update variant set plays = plays + 1 where id = ${variantId}`;
		let result = await global.db.query(query);
    }

	static async incrementViews(variantId)
	{
		let query = `update variant set views = views + 1 where id = ${variantId}`;
		let result = await global.db.query(query);
	}

	static async getVariant(id)
	{
		let query = 'select variant.*, account.username from variant join account on(variant.player_id = account.id) where variant.id = ' + id;
		let result = await global.db.query(query);
		if(result.rows.length == 0)
			return null;
		let r = result.rows[0];
		return new Variant(r.id, r.views, r.plays, r.likes, r.dislikes, r.board, r.privacy, r.title, r.duration, r.extension, r.username);
	}

	static async deleteFromTable(){
		await global.db.query('delete from "like"');
		await global.db.query('delete from Dislike');
		await global.db.query('update variant set Likes = 0, Dislikes = 0')
		await global.db.query('delete from favourite')
	}

	static async getOwnVariants(id)
	{
		let query = `select variant.* from variant where player_id = '${id}'`;
		let result = await global.db.query(query);
		return result;
	}

	static async getFavouriteVariants(id)
	{
		let query = `select variant.* from variant, favourite where favourite.player_id = '${id}' and favourite.variant_id = variant.id;`
		let result = await global.db.query(query);
		return result;
	}

	static async getStats(player_id, id){



        let resultLike= await global.db.query(`SELECT COUNT("player_id")
                                        FROM "like"
                                        WHERE player_id = ${player_id} and variant_id =  '${id}' ;`)
        let resultDislike= await global.db.query(`SELECT COUNT("player_id")
                                        FROM Dislike
                                        WHERE player_id = ${player_id} and variant_id =  '${id}' ;`)
        let Liked, Disliked, favourite
        if (resultLike.rows[0].count!='0'){
                console.log("prososam uvjet u getStats za row");
                Liked="Liked"
                Disliked="Dislike"
            }
        else if(resultDislike.rows[0].count!='0'){
                Disliked="Disliked"
                Liked="Like"
            }

        else{
                Disliked="Dislike"
                Liked="Like"
        }

		let resultfav= await global.db.query(`SELECT COUNT("player_id")
												FROM favourite
												WHERE player_id = ${player_id} and variant_id =  '${id}' ;`)
		if (resultfav.rows[0].count=='0') favourite="Favourite"
		if (resultfav.rows[0].count!='0') favourite= "Unfavourite"


        return [Liked, Disliked, favourite];
	}

	// for VariantController
	static async LikeVariant(variantId, userId)
	{
		let dobar = {}

		let query= `SELECT * from Dislike where player_id = ${userId} and variant_id=${variantId}`
		let result = await global.db.query(query);

		if(result.rows.length > 0){

			await global.db.query(`UPDATE variant SET Dislikes = Dislikes - 1 WHERE id = ${variantId}`)

			await global.db.query(`delete from Dislike WHERE player_id = ${userId} and variant_id=${variantId}`)
		}


		await global.db.query(`INSERT INTO "like" values (${userId},${variantId})`)
		await global.db.query(`UPDATE variant SET Likes = Likes + 1 WHERE id = ${variantId}`)
		dobar.ok=true
		return dobar
	}

	static async unLikeVariant(variantId, userId)
	{
		let dobar = {}
		await global.db.query(`delete from "like" WHERE player_id = ${userId} and variant_id=${variantId}`)
		await global.db.query(`UPDATE variant SET Likes = Likes - 1 WHERE id = ${variantId}`)
		dobar.ok=true
		return dobar
	}

	// for VariantController
	static async DislikeVariant(variantId, userId)
	{
		let query= `SELECT * from "like" where player_id = ${userId} and variant_id=${variantId}`
		let result = await global.db.query(query);

		let dobar = {}

		if(result.rows.length > 0){

			await global.db.query(`UPDATE variant SET Likes = Likes - 1 WHERE id = ${variantId}`)

			await global.db.query(`delete from "like" WHERE player_id = ${userId} and variant_id=${variantId}`)
		}

		await global.db.query(`INSERT INTO Dislike values (${userId},${variantId})`)
		await global.db.query(`UPDATE variant SET Dislikes = Dislikes + 1 WHERE id = ${variantId}`)

		dobar.ok=true
		return dobar
	}

	static async unDislikeVariant(variantId, userId)
	{
		let dobar = {}
		await global.db.query(`delete from Dislike WHERE player_id = ${userId} and variant_id=${variantId}`)
		await global.db.query(`UPDATE variant SET Dislikes = Dislikes - 1 WHERE id = ${variantId}`)
		dobar.ok=true
		return dobar
	}

	// for VariantController
	static async favouriteVariant(variantId, userId)
	{

		let query = `insert into favourite(player_id, variant_id) values ('${userId}','${variantId}')`
		let result = await global.db.query(query)
		return result;
	}

	static async removeFromFavourites(variantId, userId)
	{
		let query = `delete from favourite where variant_id = '${variantId}' and player_id = '${userId}'`;
		let result = await global.db.query(query);

		return result;
	}

	static async getAllPublicVariantIds()
	{
		let query = "select variant.id from variant where privacy = false";
		let result = await global.db.query(query);

		let idArray = [];

		for(let row of result.rows)
			idArray.push(row.id);

		return idArray;
	}

	static async getBestRatedVariantIds(howMany)
	{
		let result = await global.db.query(`SELECT variant.id FROM variant where privacy = false ORDER BY (Likes - Dislikes) DESC fetch first ${howMany} rows only`)

		let idArray = [];

		for(let row of result.rows)
			idArray.push(row.id);

		return idArray;
	}

	// for HomeController - returns int[]
	static async getMostPopularVariantIds(howMany)
	{
		let result = await global.db.query(`SELECT variant.id FROM variant where privacy = false ORDER BY views DESC fetch first ${howMany} rows only`)

		let idArray = [];

		for(let row of result.rows)
			idArray.push(row.id);

		return idArray;
	}

	// for HomeController - returns int[]
	static async getNewestVariantIds(howMany)
	{
		let result = await global.db.query(`SELECT variant.id FROM variant where privacy = false ORDER BY id DESC fetch first ${howMany} rows only`)

		let idArray = [];

		for(let row of result.rows)
			idArray.push(row.id);

		return idArray;
	}


	// for AdminController
	static async getAllVariants()
	{
		let query = 'select variant.*, account.username from variant join account on(variant.player_id = account.id)';
		let result = await global.db.query(query);
		if(result.rows.length == 0)
			return null;

		let variants = [];
		for(let r of result.rows)
		{
			variants.push(new Variant(r.id, r.views, r.plays, r.Likes, r.Dislikes, r.board, r.privacy, r.title, r.duration, r.extension, r.username));
		}
		return variants;
	}

	// for AdminController and ProfileController
	static async deleteVariant(id)
	{
		let query = `delete from variant where id = '${id}'`;
		let result = await global.db.query(query);
		return result;

	}

	// for ProfileController
	static async toggleVariantPrivacy(id)
	{
		let query = `select privacy from variant where id = '${id}'`;
		let privacy = await global.db.query(query)
		console.log(privacy);
		if (privacy.rows[0].privacy == true){
			let query2 = `update variant set privacy = false where id = '${id}'`;
			let result = await global.db.query(query2);
			return result;
		}
		else{
			let query2 = `update variant set privacy = true where id = '${id}'`;
			let result = await global.db.query(query2);
			return result;
		}

	}

	static async recreateVariantImages()
    {
        let variants = await this.getAllVariants();
        if(!variants)
        	return;

        const fs = require('fs')
        const { createCanvas, loadImage } = require('canvas');
        let figureImages = [];

        for(let variant of variants)
        {
        	const variantId = variant.id;
        	const board = variant.board;
        	const duration = variant.duration;
        	const extension = variant.extension;

        	const width = 800;
	        const height = 1000;
	        const heightForBoard = 800;

	        var canvas = createCanvas(width, height);
	        var context = canvas.getContext('2d');

	        let fieldWidth  = Math.floor(width  / board.width);
	        let fiedlHeight = Math.floor(heightForBoard / board.height);
	        let fieldSize;

	        if(fieldWidth > fiedlHeight)
	            fieldSize = fiedlHeight;
	        else
	            fieldSize = fieldWidth;

	        let xOffset = Math.floor((width - fieldSize * board.width) / 2);
	        let yOffset = Math.floor((heightForBoard - fieldSize * board.height) / 2);

	        const black = "#b58863";
	        const white = "#f0d9b5";
	        const removed = "grey";

	        // Fill background
	        context.fillStyle = "#fccc74";
	        context.fillRect(0, 0, width, height);
	        // Fill area for board
	        context.fillStyle = "#855E42";
	        context.fillRect(0, 0, width, heightForBoard);

	        for(let i = 0; i < board.width * board.height; i++)
	        {
	            let field = board.fields[i];

	            if(field.type == "white")
	                context.fillStyle = white;
	            else if(field.type == "black")
	                context.fillStyle = black;
	            if(field.removed)
	                context.fillStyle = removed;

	            let x = xOffset + fieldSize * (i % board.width);
	            let y = yOffset + fieldSize * Math.floor(i / board.width);

	            context.fillRect(x, y, fieldSize, fieldSize);

	            let figure = field.figure;
	            if(figure)
	            {
	                if(figureImages[figure] == undefined)
	                {
	                    figureImages[figure] = await loadImage(`./public/images/chess/${figure[0]}/${figure[1]}.png`);
	                }
	                context.drawImage(figureImages[figure], x, y, fieldSize, fieldSize);
	            }
	        }

	        let timeFormat = `${duration}s + ${extension}s`

	        context.fillStyle = "#000000";
	        context.font = "64px Arial";
	        context.textAlign = "center";
	        context.textBaseline = 'middle';
	        context.fillText(timeFormat, width / 2, heightForBoard + (height-heightForBoard) / 2);

	        const buffer = canvas.toBuffer('image/png');
	        fs.writeFileSync(`./public/images/variant/${variantId}.png`, buffer);
        }
    }
}

module.exports = Variant;