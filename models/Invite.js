class Invite
{
	constructor(id, senderId, senderUsername, variantId, variantName)
	{
		this.id = id;
		this.senderId = senderId;
		this.senderUsername = senderUsername;
		this.variantId = variantId;
		this.variantName = variantName;
	}

	static async getReceivedInvites(playerId)
	{
		let query = `select invitation.id, sender_id, variant_id, variant.title, account.username
					 from invitation join variant on (variant.id = variant_id)
					 join account on (account.id = sender_id)
					 where recipient_id =${playerId}`;

		let result = await global.db.query(query);
		if(result.rows.length == 0)
			return [];
		let invites = [];

		for(let row of result.rows)
		{
			invites.push(new Invite(row.id, row.sender_id, row.username, row.variant_id, row.title));
		}

		return invites;
	}

	static async createInvite(recipientUsername, senderId, variantId)
	{
		let query = `select account.id
					 from account join player on(account.id = player.id)
					 where account.username = '${recipientUsername}'`;
		let result = await global.db.query(query);
		if(result.rows.length == 0)
			return false;
		let recipientId = result.rows[0].id;
		if(recipientId == senderId)
			return false;

		query = `insert into invitation(sender_id, recipient_id, variant_id) values(${senderId}, ${recipientId}, ${variantId})`;
		result = await global.db.query(query);

		return true;
	}

	static async deleteInvite(id)
	{
		await global.db.query(`delete from invitation where id = ${id}`);
	}
}

module.exports = Invite;